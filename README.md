## [`dune-archiso.gitlab.io`](https://dune-archiso.gitlab.io)

#### Contents:

- [docs](https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/tree/main/docs)
- [templates](https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/tree/main/templates)

[](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41488#note_288067302)

<!-- ```console
nm --print-file-name --dynamic /usr/lib/libtirpc.so
/usr/lib/libtirpc.so:                 U abort@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U accept@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U alarm@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __asprintf_chk@GLIBC_2.8
/usr/lib/libtirpc.so:                 U __assert_fail@GLIBC_2.2.5
/usr/lib/libtirpc.so:00000000000122b0 T authdes_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000122c0 T authdes_pk_create@@TIRPC_0.3.3
/usr/lib/libtirpc.so:00000000000122d0 T authdes_seccreate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015e30 T _authenticate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d440 T authgss_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d6b0 T authgss_create_default@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d860 T authgss_free_private_data@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d7d0 T authgss_get_private_data@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d8f0 T authgss_service@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000081c0 T authnone_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000087e0 T authunix_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000008ad0 T authunix_create_default@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U bind@GLIBC_2.2.5
/usr/lib/libtirpc.so:0000000000009380 T bindresvport@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000008ee0 T bindresvport_sa@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U calloc@GLIBC_2.2.5
/usr/lib/libtirpc.so:00000000000121a0 T callrpc@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000121e0 T clnt_broadcast@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c1f0 T clnt_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000be20 T clnt_create_timed@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c1d0 T clnt_create_vers@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c020 T clnt_create_vers_timed@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000b3a0 T clnt_dg_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c740 T clnt_pcreateerror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c530 T clnt_perrno@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c4e0 T clnt_perror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000caf0 T clnt_raw_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012110 T clntraw_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c560 T clnt_spcreateerror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c210 T clnt_sperrno@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c230 T clnt_sperror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000120f0 T clnttcp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000b9c0 T clnt_tli_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000c200 T clnt_tp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000bca0 T clnt_tp_create_timed@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012070 T clntudp_bufcreate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000120d0 T clntudp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000122e0 T clntunix_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000dd80 T clnt_vc_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U close@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U connect@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __ctype_b_loc@GLIBC_2.3
/usr/lib/libtirpc.so:                 w __cxa_finalize@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U endgrent@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000000eab0 T endnetconfig@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f260 T endnetpath@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U __errno_location@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fclose@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fcntl@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __fdelt_chk@GLIBC_2.15
/usr/lib/libtirpc.so:                 U feof@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fflush@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fgets@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fopen@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __fprintf_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U fputc@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fread@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U free@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U freeaddrinfo@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U freeifaddrs@GLIBC_2.3
/usr/lib/libtirpc.so:000000000000f080 T freenetconfigent@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U fseek@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fsync@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U ftell@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U fwrite@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getaddrinfo@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __getdelim@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getdomainname@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getdtablesize@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getegid@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getenv@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U geteuid@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getgrent@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getgrouplist@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getgroups@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U gethostbyname@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U gethostname@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getifaddrs@GLIBC_2.3
/usr/lib/libtirpc.so:0000000000012180 T get_myaddress@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000e840 T getnetconfig@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000ec00 T getnetconfigent@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000021c00 T getnetname@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000000f390 T getnetpath@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f2e0 T _get_next_token@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U getpeername@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getpid@GLIBC_2.2.5
/usr/lib/libtirpc.so:00000000000217d0 T getpublicandprivatekey@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021a30 T getpublickey@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000002ee60 B __getpublickey_LOCAL@@TIRPC_0.3.3
/usr/lib/libtirpc.so:                 U getpwuid@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getpwuid_r@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getrlimit@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000000f4b0 T getrpcport@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U getservbyname@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getsockname@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U getsockopt@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U gettimeofday@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 w __gmon_start__
/usr/lib/libtirpc.so:                 U gss_accept_sec_context@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_acquire_cred@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:0000000000015cd0 T _gss_authenticate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U gss_canonicalize_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U GSS_C_NT_HOSTBASED_SERVICE@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U GSS_C_NT_USER_NAME@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_delete_sec_context@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_display_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_display_status@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_duplicate_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_export_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_get_mic@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_import_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_init_sec_context@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:000000000001de50 T gss_log_debug@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e9e0 T gss_log_hexdump@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e2f0 T gss_log_status@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U gss_pname_to_uid@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_release_buffer@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_release_cred@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_release_name@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_sign@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_unwrap@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_verify_mic@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_wrap@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:                 U gss_wrap_size_limit@gssapi_krb5_2_MIT
/usr/lib/libtirpc.so:0000000000021b10 T host2netname@@TIRPC_0.3.2
/usr/lib/libtirpc.so:                 U if_nametoindex@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U inet_ntoa@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U inet_ntop@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U inet_pton@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U ioctl@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __isoc99_sscanf@GLIBC_2.7
/usr/lib/libtirpc.so:                 w _ITM_deregisterTMCloneTable
/usr/lib/libtirpc.so:                 w _ITM_registerTMCloneTable
/usr/lib/libtirpc.so:0000000000021290 T key_decryptsession@@TIRPC_0.3.2
/usr/lib/libtirpc.so:00000000000210f0 T key_decryptsession_pk@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000002ee50 B __key_decryptsession_pk_LOCAL@@TIRPC_0.3.3
/usr/lib/libtirpc.so:00000000000211b0 T key_encryptsession@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021030 T key_encryptsession_pk@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000002ee58 B __key_encryptsession_pk_LOCAL@@TIRPC_0.3.3
/usr/lib/libtirpc.so:0000000000021370 T key_gendes@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000002ee48 B __key_gendes_LOCAL@@TIRPC_0.3.3
/usr/lib/libtirpc.so:0000000000021460 T key_get_conv@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000020f90 T key_secretkey_is_set@@TIRPC_0.3.2
/usr/lib/libtirpc.so:00000000000213a0 T key_setnet@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000020ed0 T key_setsecret@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000012010 T __libc_clntudp_bufcreate@@TIRPC_PRIVATE
/usr/lib/libtirpc.so:0000000000019f70 T libtirpc_set_debug@@TIRPC_PRIVATE
/usr/lib/libtirpc.so:                 U listen@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U malloc@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U memcmp@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U memcpy@GLIBC_2.14
/usr/lib/libtirpc.so:                 U __memcpy_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U memmove@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000000f130 T nc_perror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f0c0 T nc_sperror@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000022140 T netname2host@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021df0 T netname2user@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000002ec00 B _null_auth@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U openlog@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000000f880 T pmap_getmaps@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f980 T pmap_getport@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000ff20 T pmap_rmtcall@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f6e0 T pmap_set@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f7f0 T pmap_unset@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U poll@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __poll_chk@GLIBC_2.16
/usr/lib/libtirpc.so:                 U __printf_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U pthread_cond_destroy@GLIBC_2.3.2
/usr/lib/libtirpc.so:                 U pthread_cond_init@GLIBC_2.3.2
/usr/lib/libtirpc.so:                 U pthread_cond_signal@GLIBC_2.3.2
/usr/lib/libtirpc.so:                 U pthread_cond_wait@GLIBC_2.3.2
/usr/lib/libtirpc.so:                 U pthread_getspecific@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_key_create@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_key_delete@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_mutex_lock@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_mutex_unlock@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_rwlock_rdlock@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_rwlock_unlock@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_rwlock_wrlock@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_setspecific@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U pthread_sigmask@GLIBC_2.32
/usr/lib/libtirpc.so:                 U rand_r@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U read@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U realloc@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U recvfrom@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U recvmsg@GLIBC_2.2.5
/usr/lib/libtirpc.so:00000000000121c0 T registerrpc@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000013d00 T rpcb_getaddr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000013da0 T rpcb_getmaps@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014150 T rpcb_gettime@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000013ef0 T rpcb_rmtcall@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000a400 T rpc_broadcast@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000009650 T rpc_broadcast_exp@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000013390 T rpcb_set@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014330 T rpcb_taddr2uaddr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014400 T rpcb_uaddr2taddr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000013510 T rpcb_unset@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000cd40 T rpc_call@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015c90 T rpc_control@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f590 T __rpc_createerr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002e400 B rpc_createerr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000e390 T _rpc_dtablesize@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010a80 T __rpc_dtbsize@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000110b0 T __rpc_endconf@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011140 T __rpc_fd2sockinfo@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000119b0 T __rpc_fixup_addr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010b20 T __rpc_get_a_size@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010f10 T __rpc_getconf@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010b40 T __rpc_getconfip@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000191c0 T __rpc_get_local_uid@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011300 T __rpcgettp@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010af0 T __rpc_get_t_size@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001fec0 T rpc_gss_getcred@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000204b0 T rpc_gss_get_error@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020510 T rpc_gss_get_mechanisms@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020530 T rpc_gss_get_mech_info@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020120 T rpc_gss_get_principal_name@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000205d0 T rpc_gss_get_versions@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020620 T rpc_gss_is_installed@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001dd60 T rpc_gss_max_data_length@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020690 T rpc_gss_mech_to_oid@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000020810 T rpc_gss_qop_to_num@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001d950 T rpc_gss_seccreate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000200a0 T rpc_gss_set_callback@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001dc30 T rpc_gss_set_defaults@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001fe00 T rpc_gss_set_svc_name@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001fd60 T rpc_gss_svc_max_data_length@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000118c0 T __rpc_nconf2fd@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011820 T __rpc_nconf2fd_flags@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011720 T __rpc_nconf2sockinfo@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011100 T rpc_nullproc@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002ecc0 B __rpc_rawcombuf@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000017d00 T rpc_reg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011700 T __rpc_seman2socktype@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010dd0 T __rpc_setconf@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011220 T __rpc_sockinfo2netid@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011a20 T __rpc_sockisbound@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011990 T __rpc_socktype2seman@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000011370 T __rpc_taddr2uaddr_af@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000114c0 T __rpc_uaddr2taddr_af@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000223b0 T rtime@@TIRPC_0.3.2
/usr/lib/libtirpc.so:                 U select@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U sendmsg@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U sendto@GLIBC_2.2.5
/usr/lib/libtirpc.so:00000000000104f0 T _seterr_reply@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U setgrent@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000000e780 T setnetconfig@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000f170 T setnetpath@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U setsockopt@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U sigfillset@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U signal@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U sleep@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U snprintf@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __snprintf_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U socket@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __sprintf_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U __stack_chk_fail@GLIBC_2.4
/usr/lib/libtirpc.so:                 U stderr@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strcasecmp@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strchr@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strcmp@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strcpy@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __strcpy_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U strdup@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strerror@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strlen@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strncat@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strncmp@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strncpy@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strpbrk@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strrchr@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strsep@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strtok_r@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strtol@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U strtoul@GLIBC_2.2.5
/usr/lib/libtirpc.so:000000000001f230 T _svcauth_gss@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001fd00 T svcauth_gss_get_principal@@TIRPC_0.3.1
/usr/lib/libtirpc.so:000000000001f0a0 T svcauth_gss_set_svc_name@@TIRPC_0.3.1
/usr/lib/libtirpc.so:00000000000170b0 T _svcauth_none@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002e060 D svc_auth_none@@TIRPC_PRIVATE
/usr/lib/libtirpc.so:0000000000015e70 T svc_auth_reg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000017080 T _svcauth_short@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000016e30 T _svcauth_unix@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000019230 T __svc_clean_idle@@TIRPC_PRIVATE
/usr/lib/libtirpc.so:0000000000017420 T svc_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000016940 T svc_dg_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000016ce0 T svc_dg_enablecache@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000156e0 T svcerr_auth@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000155c0 T svcerr_decode@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015530 T svcerr_noproc@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000157a0 T svcerr_noprog@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015830 T svcerr_progvers@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015650 T svcerr_systemerr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015760 T svcerr_weakauth@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000017b10 T svc_exit@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000019030 T svc_fd_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012140 T svcfd_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002eb80 B svc_fdset@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015ba0 T svc_getreq@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000158c0 T svc_getreq_common@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015c00 T svc_getreq_poll@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015af0 T svc_getreqset@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002e030 D svc_maxfd@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000002eb60 B svc_max_pollfd@@TIRPC_0.3.3
/usr/lib/libtirpc.so:000000000002eb68 B svc_pollfd@@TIRPC_0.3.3
/usr/lib/libtirpc.so:00000000000177f0 T svc_raw_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012170 T svcraw_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015070 T svc_reg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015310 T svc_register@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000179d0 T svc_run@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000154a0 T svc_sendreply@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012120 T svctcp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000170c0 T svc_tli_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000017370 T svc_tp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012130 T svcudp_bufcreate@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012150 T svcudp_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012440 T svcunix_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000012630 T svcunixfd_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015260 T svc_unreg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015420 T svc_unregister@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000018d30 T svc_vc_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U sysconf@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U __syslog_chk@GLIBC_2.4
/usr/lib/libtirpc.so:00000000000118d0 T taddr2uaddr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U time@GLIBC_2.2.5
/usr/lib/libtirpc.so:0000000000000000 A TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000000000 A TIRPC_0.3.1
/usr/lib/libtirpc.so:0000000000000000 A TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000000000 A TIRPC_0.3.3
/usr/lib/libtirpc.so:0000000000000000 A TIRPC_PRIVATE
/usr/lib/libtirpc.so:0000000000011930 T uaddr2taddr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:                 U uname@GLIBC_2.2.5
/usr/lib/libtirpc.so:0000000000021a50 T user2netname@@TIRPC_0.3.2
/usr/lib/libtirpc.so:                 U __vfprintf_chk@GLIBC_2.3.4
/usr/lib/libtirpc.so:                 U __vsyslog_chk@GLIBC_2.4
/usr/lib/libtirpc.so:                 U warn@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U warnx@GLIBC_2.2.5
/usr/lib/libtirpc.so:                 U write@GLIBC_2.2.5
/usr/lib/libtirpc.so:0000000000010220 T xdr_accepted_reply@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001bb70 T xdr_array@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000008c40 T xdr_authunix_parms@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a740 T xdr_bool@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a8b0 T xdr_bytes@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010420 T xdr_callhdr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010660 T xdr_callmsg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a680 T xdr_char@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000021590 T xdr_cryptkeyarg@@TIRPC_0.3.2
/usr/lib/libtirpc.so:00000000000215d0 T xdr_cryptkeyarg2@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021620 T xdr_cryptkeyres@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000010310 T xdr_des_block@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001bdc0 T xdr_double@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a7d0 T xdr_enum@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001bd30 T xdr_float@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000019fe0 T xdr_free@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000216e0 T xdr_getcredres@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000001ae20 T xdr_hyper@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a030 T xdr_int@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a420 T xdr_int16_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a1e0 T xdr_int32_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ac30 T xdr_int64_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a550 T xdr_int8_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000021550 T xdr_keybuf@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021730 T xdr_key_netstarg@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021780 T xdr_key_netstres@@TIRPC_0.3.2
/usr/lib/libtirpc.so:0000000000021530 T xdr_keystatus@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000001a140 T xdr_long@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ae40 T xdr_longlong_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001c050 T xdrmem_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014990 T xdr_netbuf@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000021570 T xdr_netnamestr@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000001a9e0 T xdr_netobj@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a7e0 T xdr_opaque@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010190 T xdr_opaque_auth@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000fb00 T xdr_pmap@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000fbb0 T xdr_pmaplist@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000fcf0 T xdr_pmaplist_ptr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001c140 T xdr_pointer@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ae60 T xdr_quad_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001b490 T xdrrec_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001b750 T xdrrec_endofrecord@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001b670 T xdrrec_eof@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ba20 T xdrrec_skiprecord@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001c080 T xdr_reference@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000100d0 T xdr_rejected_reply@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000010370 T xdr_replymsg@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000fd00 T xdr_rmtcall_args@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000000fe40 T xdr_rmtcallres@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000144f0 T xdr_rpcb@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014570 T xdr_rpcb_entry@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014710 T xdr_rpcb_entry_list_ptr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014700 T xdr_rpcblist@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014600 T xdr_rpcblist_ptr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014810 T xdr_rpcb_rmtcallargs@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014940 T xdr_rpcb_rmtcallres@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014b60 T xdr_rpcbs_addrlist@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014c20 T xdr_rpcbs_addrlist_ptr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014bf0 T xdr_rpcbs_proc@@TIRPC_0.3.0
/usr/lib/libtirpc.so:00000000000149e0 T xdr_rpcbs_rmtcalllist@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014c50 T xdr_rpcbs_rmtcalllist_ptr@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014c80 T xdr_rpcb_stat@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014cf0 T xdr_rpcb_stat_byvers@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e060 T xdr_rpc_gss_cred@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e990 T xdr_rpc_gss_data@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e160 T xdr_rpc_gss_init_args@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001e1d0 T xdr_rpc_gss_init_res@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a300 T xdr_short@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001c4e0 T xdr_sizeof@@TIRPC_0.3.3
/usr/lib/libtirpc.so:000000000001c3c0 T xdrstdio_create@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001aa90 T xdr_string@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a6e0 T xdr_u_char@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ae30 T xdr_u_hyper@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a0c0 T xdr_u_int@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a4b0 T xdr_u_int16_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a540 T xdr_uint16_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a270 T xdr_u_int32_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a2f0 T xdr_uint32_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ad20 T xdr_u_int64_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ae10 T xdr_uint64_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a5e0 T xdr_u_int8_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a670 T xdr_uint8_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a190 T xdr_u_long@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ae50 T xdr_u_longlong_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001aa00 T xdr_union@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000021670 T xdr_unixcred@@TIRPC_0.3.2
/usr/lib/libtirpc.so:000000000001ae70 T xdr_u_quad_t@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a390 T xdr_u_short@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001bcb0 T xdr_vector@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001a020 T xdr_void@@TIRPC_0.3.0
/usr/lib/libtirpc.so:000000000001ac20 T xdr_wrapstring@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000014ed0 T xprt_register@@TIRPC_0.3.0
/usr/lib/libtirpc.so:0000000000015050 T xprt_unregister@@TIRPC_0.3.0
```

```console
readelf --dynamic /usr/lib/libalberta_1d.so

Dynamic section at offset 0xcbd20 contains 28 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libdl.so.2]
 0x0000000000000001 (NEEDED)             Shared library: [libtirpc.so.3]
 0x0000000000000001 (NEEDED)             Shared library: [libm.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000e (SONAME)             Library soname: [libalberta_1d.so.4]
 0x000000000000000c (INIT)               0x11000
 0x000000000000000d (FINI)               0xa9e38
 0x0000000000000019 (INIT_ARRAY)         0xca290
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0xca298
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x000000006ffffef5 (GNU_HASH)           0x300
 0x0000000000000005 (STRTAB)             0x4318
 0x0000000000000006 (SYMTAB)             0x1060
 0x000000000000000a (STRSZ)              8672 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000003 (PLTGOT)             0xcd000
 0x0000000000000002 (PLTRELSZ)           6672 (bytes)
 0x0000000000000014 (PLTREL)             RELA
 0x0000000000000017 (JMPREL)             0xf0b8
 0x0000000000000007 (RELA)               0x69e8
 0x0000000000000008 (RELASZ)             34512 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x6938
 0x000000006fffffff (VERNEEDNUM)         3
 0x000000006ffffff0 (VERSYM)             0x64f8
 0x000000006ffffff9 (RELACOUNT)          1398
 0x0000000000000000 (NULL)               0x0
``` -->

<!-- ```console
readelf --dynamic /usr/lib/libalberta_1d.so

Dynamic section at offset 0xcbd20 contains 28 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libdl.so.2]
 0x0000000000000001 (NEEDED)             Shared library: [libtirpc.so.3]
 0x0000000000000001 (NEEDED)             Shared library: [libm.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000e (SONAME)             Library soname: [libalberta_1d.so.4]
 0x000000000000000c (INIT)               0x11000
 0x000000000000000d (FINI)               0xa9e38
 0x0000000000000019 (INIT_ARRAY)         0xca290
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0xca298
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x000000006ffffef5 (GNU_HASH)           0x300
 0x0000000000000005 (STRTAB)             0x4318
 0x0000000000000006 (SYMTAB)             0x1060
 0x000000000000000a (STRSZ)              8672 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000003 (PLTGOT)             0xcd000
 0x0000000000000002 (PLTRELSZ)           6672 (bytes)
 0x0000000000000014 (PLTREL)             RELA
 0x0000000000000017 (JMPREL)             0xf0b8
 0x0000000000000007 (RELA)               0x69e8
 0x0000000000000008 (RELASZ)             34512 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x6938
 0x000000006fffffff (VERNEEDNUM)         3
 0x000000006ffffff0 (VERSYM)             0x64f8
 0x000000006ffffff9 (RELACOUNT)          1398
 0x0000000000000000 (NULL)               0x0
``` -->
