## man g++

### `-Wall`

This enables all the warnings about constructions that some users consider questionable, and that are easy to avoid
(or modify to prevent the warning), even in conjunction with macros. This also enables some language-specific
warnings described in C++ Dialect Options and Objective-C and Objective-C++ Dialect Options.

### `-Wunused`

All the above -Wunused options combined.

In order to get a warning about an unused function parameter, you must either specify -Wextra -Wunused (note that
-Wall implies -Wunused), or separately specify -Wunused-parameter.

### -Wmissing-include-dirs (C, C++, Objective-C and Objective-C++ only)

Warn if a user-supplied include directory does not exist.

### -Wcast-align

Warn whenever a pointer is cast such that the required alignment of the target is increased. For example, warn if a
"char _" is cast to an "int _" on machines where integers can only be accessed at two- or four-byte boundaries.

### -Wmissing-braces

Warn if an aggregate or union initializer is not fully bracketed. In the following example, the initializer for "a"
is not fully bracketed, but that for "b" is fully bracketed.

### -Wmissing-field-initializers

Warn if a structure's initializer has some fields missing. For example, the following code causes such a warning,
because "x.h" is implicitly zero:

### -Wsign-compare

Warn when a comparison between signed and unsigned values could produce an incorrect result when the signed value is
converted to unsigned. In C++, this warning is also enabled by -Wall. In C, it is also enabled by -Wextra.

### -fdiagnostics-color=always and -fdiagnostics-color=never, respectively.

The colors are defined by the environment variable GCC_COLORS. Its value is a colon-separated list of capabilities
and Select Graphic Rendition (SGR) substrings. SGR commands are interpreted by the terminal or terminal emulator.
(See the section in the documentation of your text terminal for permitted values and their meanings as character
attributes.) These substring values are integers in decimal representation and can be concatenated with semicolons.
Common values to concatenate include 1 for bold, 4 for underline, 5 for blink, 7 for inverse, 39 for default
foreground color, 30 to 37 for foreground colors, 90 to 97 for 16-color mode foreground colors, 38;5;0 to 38;5;255
for 88-color and 256-color modes foreground colors, 49 for default background color, 40 to 47 for background colors,
100 to 107 for 16-color mode background colors, and 48;5;0 to 48;5;255 for 88-color and 256-color modes background
colors.

#### Other options such as -ffp-contract, -fno-strict-overflow, -fwrapv, -fno-trapv or -fno-strict-aliasing are passed

through to the link stage and merged conservatively for conflicting translation units. Specifically
-fno-strict-overflow, -fwrapv and -fno-trapv take precedence; and for example -ffp-contract=off takes precedence over
-ffp-contract=fast. You can override them at link time.

#### -fstrict-overflow

This option implies -fno-wrapv -fno-wrapv-pointer and when negated implies -fwrapv -fwrapv-pointer.

#### -ffinite-math-only

Allow optimizations for floating-point arithmetic that assume that arguments and results are not NaNs or +-Infs.

This option is not turned on by any -O option since it can result in incorrect output for programs that depend on an
exact implementation of IEEE or ISO rules/specifications for math functions. It may, however, yield faster code for
programs that do not require the guarantees of these specifications.

The default is -fno-finite-math-only.

#### -fno-enforce-eh-specs

Don't generate code to check for violation of exception specifications at run time. This option violates the C++
standard, but may be useful for reducing code size in production builds, much like defining "NDEBUG". This does not
give user code permission to throw exceptions in violation of the exception specifications; the compiler still
optimizes based on the specifications, so throwing an unexpected exception results in undefined behavior at run time.

#### -O3 Optimize yet more

-O3 turns on all optimizations specified by -O2 and also turns on the following optimization
flags:

#### -march=name

Specify the name of the target architecture and, optionally, one or more feature modifiers. This option has the form
-march=arch{+[no]feature}\*.

#### -funroll-loops

Unroll loops whose number of iterations can be determined at compile time or upon entry to the loop. -funroll-loops
implies -frerun-cse-after-loop, -fweb and -frename-registers. It also turns on complete loop peeling (i.e. complete
removal of loops with a small constant number of iterations). This option makes code larger, and may or may not make
it run faster.

#### -gvmslevel

Request debugging information and also use level to specify how much information. The default level is 2.

Level 0 produces no debug information at all. Thus, -g0 negates -g.

## cmake-variables

#### CMAKE_C_COMPILER

set(CMAKE_C_COMPILER /full/path/to/qcc --arg1 --arg2)

or

$ cmake ... -DCMAKE_C_COMPILER='qcc;--arg1;--arg2'

#### CMAKE\_<LANG>\_COMPILER_VERSION

Compiler version string.

Compiler version in major[.minor[.patch[.tweak]]] format. This variable is not guaranteed to be defined for all compil‐
ers or languages.

For example CMAKE_C_COMPILER_VERSION and CMAKE_CXX_COMPILER_VERSION might indicate the respective C and C++ compiler ver‐
sion.

#### CMAKE GENERATORS

Command-Line Build Tool Generators
These generators support command-line build tools. In order to use them, one must launch CMake from a command-line
prompt whose environment is already configured for the chosen compiler and build tool.

Unix Makefiles

Generates standard UNIX makefiles.

A hierarchy of UNIX makefiles is generated into the build tree. Use any standard UNIX-style make program to build the
project through the all target and install the project through the install (or install/strip) target.

For each subdirectory sub/dir of the project a UNIX makefile will be created, containing the following targets:

all Depends on all targets required by the subdirectory.

install
Runs the install step in the subdirectory, if any.

install/strip
Runs the install step in the subdirectory followed by a CMAKE_STRIP command, if any.

      The  CMAKE_STRIP variable will contain the platform's strip utility, which removes symbols information from gener‐
      ated binaries.

test Runs the test step in the subdirectory, if any.

package
Runs the package step in the subdirectory, if any.

Ninja Generators
Ninja
Generates build.ninja files.

A build.ninja file is generated into the build tree. Use the ninja program to build the project through the all target
and install the project through the install (or install/strip) target.

For each subdirectory sub/dir of the project, additional targets are generated:

sub/dir/all
New in version 3.6: Depends on all targets required by the subdirectory.

sub/dir/install
New in version 3.7: Runs the install step in the subdirectory, if any.

sub/dir/install/strip
New in version 3.7: Runs the install step in the subdirectory followed by a CMAKE_STRIP command, if any.

The CMAKE_STRIP variable will contain the platform's strip utility, which removes symbols information from generated
binaries.

sub/dir/test
New in version 3.7: Runs the test step in the subdirectory, if any.

sub/dir/package
New in version 3.7: Runs the package step in the subdirectory, if any.

#### CMAKE_MAKE_PROGRAM

Tool that can launch the native build system. The value may be the full path to an executable or just the tool name if
it is expected to be in the PATH.

#### CMAKE\_<LANG>\_FLAGS

Flags for all build types.

<LANG> flags used regardless of the value of CMAKE_BUILD_TYPE.

This is initialized for each language from environment variables:

• CMAKE_C_FLAGS: Initialized by the CFLAGS environment variable.

• CMAKE_CXX_FLAGS: Initialized by the CXXFLAGS environment variable.

• CMAKE_CUDA_FLAGS: Initialized by the CUDAFLAGS environment variable.

• CMAKE_Fortran_FLAGS: Initialized by the FFLAGS environment variable.

Look more man cmake-foo

#### -std=

Determine the language standard. This option is currently only supported when compiling C or C++.

The compiler can accept several base standards, such as c90 or c++98, and GNU dialects of those standards, such as
gnu90 or gnu++98. When a base standard is specified, the compiler accepts all programs following that standard plus
those using GNU extensions that do not contradict it. For example, -std=c90 turns off certain features of GCC that
are incompatible with ISO C90, such as the "asm" and "typeof" keywords, but not other GNU extensions that do not have
a meaning in ISO C90, such as omitting the middle term of a "?:" expression. On the other hand, when a GNU dialect of
a standard is specified, all features supported by the compiler are enabled, even when those features change the
meaning of the base standard. As a result, some strict-conforming programs may be rejected. The particular standard
is used by -Wpedantic to identify which features are GNU extensions given that version of the standard. For example
-std=gnu90 -Wpedantic warns about C++ style // comments, while -std=gnu99 -Wpedantic does not.

#### -pthread

Define additional macros required for using the POSIX threads library. You should use this option consistently for
both compilation and linking. This option is supported on GNU/Linux targets, most other Unix derivatives, and also
on x86 Cygwin and MinGW targets.

#### -rdynamic

Pass the flag -export-dynamic to the ELF linker, on targets that support it. This instructs the linker to add all
symbols, not only used ones, to the dynamic symbol table. This option is needed for some uses of "dlopen" or to allow
obtaining backtraces from within a program.

tbb /usr/lib/libtbb.so
tbb /usr/lib/libtbbmalloc.so

gmp /usr/lib/libgmp.so
gmp /usr/lib/libgmpxx.so

openblas-lapack /usr/lib/liblapack.so
openblas-lapack /usr/lib/libblas.so

/usr/lib/libdunecommon.so

Look more man g++
