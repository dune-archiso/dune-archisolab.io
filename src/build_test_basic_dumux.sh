#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  # local EXTRA_PACKAGES="opm-grid dune-alugrid dune-foamgrid dune-functions dune-subgrid suitesparse gnuplot"
  # sudo pacman -S ${EXTRA_PACKAGES} --needed --noconfirm >/dev/null 2>&1
  tar -I zstd -xf "${CI_PROJECT_DIR}"/cache.tar.zst
  rm "${CI_PROJECT_DIR}"/cache.tar.zst
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/dumux/ "${CI_PROJECT_DIR}"/dumux
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/build-cmake/ "${CI_PROJECT_DIR}"/build-cmake
}

function build_tests() {
  install
  pacman -Qi dumux
  # cmake -S "${CI_PROJECT_DIR}"/dumux -B "${CI_PROJECT_DIR}"/build-cmake
  cmake --build "${CI_PROJECT_DIR}"/build-cmake --target build_tests
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/cache.tar.zst ${CI_PROJECT_DIR}/{dumux,build-cmake}
}

build_tests
