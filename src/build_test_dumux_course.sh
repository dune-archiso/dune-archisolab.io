#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  sudo pacman -S dune-alugrid dune-foamgrid dune-subgrid gnuplot suitesparse --needed --noconfirm >/dev/null 2>&1
}

function clone_tests() {
  # sudo pacman -Syuq --needed --noconfirm git >/dev/null 2>&1
  local BASE_URL=https://git.iws.uni-stuttgart.de/dumux-repositories
  local TEST_REPO="${BASE_URL}"/dumux-course
  local BRANCH=3.8.0

  echo -e "Cloning DuMux course from ${BRANCH}"

  git config --global advice.detachedHead false
  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch ${BRANCH} \
    "${TEST_REPO}".git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/dumux-course/exercises"

  ls -l "${CI_PROJECT_DIR}"/dumux-course/exercises
  # sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function apply_patch() {
  sed -i 's/SOURCES main.cc/SOURCES main.cc TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux-course/exercises/solution/exercise-fluidsystem/CMakeLists.txt
  sed -i 's/NAME exercise_fluidsystem_b_solution/NAME exercise_fluidsystem_b_solution TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux-course/exercises/solution/exercise-fluidsystem/CMakeLists.txt
  sed -i 's/TARGET exercisefractures_solution/TARGET exercisefractures_solution TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux-course/exercises/solution/exercise-fractures/CMakeLists.txt
}

function build_tests() {
  install
  clone_tests
  apply_patch
  pacman -Qi dumux
  cmake -S "${CI_PROJECT_DIR}"/dumux-course -B "${CI_PROJECT_DIR}"/build-cmake -Wno-dev
  cmake --build "${CI_PROJECT_DIR}"/build-cmake --target build_tests
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/cache.tar.zst ${CI_PROJECT_DIR}/{dumux-course,build-cmake}
}

build_tests
