#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  # local EXTRA_PACKAGES="opm-grid dune-alugrid dune-foamgrid dune-functions dune-subgrid suitesparse gnuplot"
  # sudo pacman -S ${EXTRA_PACKAGES} --needed --noconfirm >/dev/null 2>&1
}

function clone_tests() {
  # sudo pacman -Syuq --needed --noconfirm git >/dev/null 2>&1
  local BASE_URL=https://git.iws.uni-stuttgart.de/dumux-repositories
  local TEST_REPO="${BASE_URL}"/dumux
  local BRANCH=3.8.0-rc1

  echo -e "Cloning DuMux tests from ${BRANCH}"

  git config --global advice.detachedHead false
  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch ${BRANCH} \
    "${TEST_REPO}".git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/dumux/test"

  ls -l "${CI_PROJECT_DIR}"/dumux/test
  # sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function apply_patch() {
  sed -i 's/#include <opm\/parser\/eclipse\/Parser\/Parser.hpp>/#include <opm\/input\/eclipse\/Parser\/Parser.hpp>/' "${CI_PROJECT_DIR}"/dumux/dumux/io/grid/cpgridmanager.hh
  sed -i 's/#include <opm\/parser\/eclipse\/Parser\/ParseContext.hpp>/#include <opm\/input\/eclipse\/Parser\/ParseContext.hpp>/' "${CI_PROJECT_DIR}"/dumux/dumux/io/grid/cpgridmanager.hh
  sed -i 's/#include <opm\/parser\/eclipse\/Deck\/Deck.hpp>/#include <opm\/input\/eclipse\/Deck\/Deck.hpp>/' "${CI_PROJECT_DIR}"/dumux/dumux/io/grid/cpgridmanager.hh
  sed -i 's/#include <opm\/parser\/eclipse\/EclipseState\/EclipseState.hpp>/#include <opm\/input\/eclipse\/EclipseState\/EclipseState.hpp>/' "${CI_PROJECT_DIR}"/dumux/dumux/io/grid/cpgridmanager.hh

  sed -i 's/#include <opm\/parser\/eclipse\/Deck\/Deck.hpp>/#include <opm\/input\/eclipse\/Deck\/Deck.hpp>/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PORO").getRawDoubleData()/getKeywordList("PORO").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PERMX").getRawDoubleData()/getKeywordList("PERMX").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PERMZ").getRawDoubleData()/getKeywordList("PERMZ").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/TARGET test_shallowwater_poiseuilleflow_unstructured/TARGET test_shallowwater_poiseuilleflow_unstructured TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux/test/freeflow/shallowwater/poiseuilleflow/CMakeLists.txt
}

function configure_tests() {
  install
  clone_tests
  apply_patch
  pacman -Qi dumux
  cmake -S "${CI_PROJECT_DIR}"/dumux -B "${CI_PROJECT_DIR}"/build-cmake -Wno-dev
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/cache.tar.zst ${CI_PROJECT_DIR}/{dumux,build-cmake}
}

configure_tests
