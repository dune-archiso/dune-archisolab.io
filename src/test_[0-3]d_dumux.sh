#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  sudo pacman -S dune-alugrid dune-foamgrid dune-subgrid dune-functions suitesparse gnuplot --needed --noconfirm >/dev/null 2>&1
  ls -l "${CI_PROJECT_DIR}"
}

function clone_tests() {
  # sudo pacman -Syuq --needed --noconfirm git >/dev/null 2>&1
  local BASE_URL=https://git.iws.uni-stuttgart.de/dumux-repositories
  local TEST_REPO="${BASE_URL}"/dumux
  local BRANCH=3.8.0-rc1

  echo -e "Cloning DuMux tests from ${BRANCH}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch ${BRANCH} \
    "${TEST_REPO}".git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/dumux/test"

  ls -l "${CI_PROJECT_DIR}"/dumux/test
  # sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function apply_patch() {
  sed -i 's/#include <opm\/parser\/eclipse\/Deck\/Deck.hpp>/#include <opm\/input\/eclipse\/Deck\/Deck.hpp>/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PORO").getRawDoubleData()/getKeywordList("PORO").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PERMX").getRawDoubleData()/getKeywordList("PERMX").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/getKeyword("PERMZ").getRawDoubleData()/getKeywordList("PERMZ").back()->getRawDoubleData()/' "${CI_PROJECT_DIR}"/dumux/test/porousmediumflow/2p/cornerpoint/spatialparams.hh
  sed -i 's/TARGET test_shallowwater_poiseuilleflow_unstructured/TARGET test_shallowwater_poiseuilleflow_unstructured TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux/test/freeflow/shallowwater/poiseuilleflow/CMakeLists.txt
  sed -i 's/TARGET test_shallowwater_poiseuilleflow_unstructured/TARGET test_shallowwater_poiseuilleflow_unstructured TIMEOUT 3000/' "${CI_PROJECT_DIR}"/dumux/test/freeflow/shallowwater/poiseuilleflow/CMakeLists.txt
}

function test_basic_installation() {
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  ls -l "${CI_PROJECT_DIR}"/build-cmake
  ctest -R test_[0-3]d --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  ls -l "${CI_PROJECT_DIR}"/build-cmake
}

install
clone_tests
apply_patch
test_basic_installation
