#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  sudo pacman -S dune-alugrid dune-foamgrid dune-subgrid gnuplot suitesparse --needed --noconfirm >/dev/null 2>&1
  tar -I zstd -xf "${CI_PROJECT_DIR}"/cache.tar.zst
  rm "${CI_PROJECT_DIR}"/cache.tar.zst
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/dumux-course/ "${CI_PROJECT_DIR}"/dumux-course
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/build-cmake/ "${CI_PROJECT_DIR}"/build-cmake
  ls -l "${CI_PROJECT_DIR}"
}

function test_course_installation() {
  ls -l "${CI_PROJECT_DIR}"/build-cmake
  ctest --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  ls -l "${CI_PROJECT_DIR}"/build-cmake
}

install
test_course_installation
