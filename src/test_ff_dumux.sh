#!/usr/bin/env bash

# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-test-installation/-/blob/main/.gitlab-ci.yml

function install() {
  sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  # sudo pacman -S dune-alugrid dune-foamgrid dune-subgrid dune-functions suitesparse gnuplot --needed --noconfirm >/dev/null 2>&1
  ls -l "${CI_PROJECT_DIR}"
  tar -I zstd -xf "${CI_PROJECT_DIR}"/cache.tar.zst
  rm "${CI_PROJECT_DIR}"/cache.tar.zst
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/dumux/ "${CI_PROJECT_DIR}"/dumux
  mv "${CI_PROJECT_DIR}"/builds/dune-archiso/repository/dumux/build-cmake/ "${CI_PROJECT_DIR}"/build-cmake
}

function test_basic_installation() {
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  ls -l "${CI_PROJECT_DIR}"/build-cmake
  ctest --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake # --verbose
  # ctest -R test_0 --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  # ctest -R test_1 --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  # ctest -R test_2 --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  # ctest -R test_3 --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  # ctest -R test_ff_ --verbose --output-on-failure --test-dir "${CI_PROJECT_DIR}"/build-cmake
  ls -l "${CI_PROJECT_DIR}"/build-cmake
}

install
test_basic_installation
