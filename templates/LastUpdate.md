
| Server                                                                           | Latest update (YY-MM-DD) |
| :------------------------------------------------------------------------------- | :----------------------: |
| [`mirrors.aliyun.com/arch4edu`](mirrors.aliyun.com/arch4edu)                     |        2022-06-14        |
| [`mirrors.tencent.com/arch4edu`](mirrors.tencent.com/arch4edu)                   |        2022-06-14        |
| [`mirrors.tuna.tsinghua.edu.cn/arch4edu`](mirrors.tuna.tsinghua.edu.cn/arch4edu) |        2022-06-13        |
| [`mirrors.bfsu.edu.cn/arch4edu`](mirrors.bfsu.edu.cn/arch4edu)                   |        2022-06-13        |
| [`mirrors.nju.edu.cn/arch4edu`](mirrors.nju.edu.cn/arch4edu)                     |        2022-06-13        |
| [`mirror.lesviallon.fr/arch4edu`](mirror.lesviallon.fr/arch4edu)                 |        2022-06-13        |
| [`mirrors.pinganyun.com/arch4edu`](mirrors.pinganyun.com/arch4edu)               |        2022-06-13        |
| [`arch4edu.mirror.kescher.at`](arch4edu.mirror.kescher.at)                       |        2022-04-20        |
| [`keybase.pub/arch4edu`](keybase.pub/arch4edu)                                   |        2022-04-13        |
| [`mirrors.ynu.edu.cn/arch4edu`](mirrors.ynu.edu.cn/arch4edu)                     |        Not found         |
| [`mirror.autisten.club/arch4edu`](mirror.autisten.club/arch4edu)                 |        Not found         |