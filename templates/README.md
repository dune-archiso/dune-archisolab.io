```yml
- sudo pacman -U `echo $CI_PROJECT_DIR/x86_64/$DEPENDENCIES_PACKAGE` --noconfirm && rm -rf $CI_PROJECT_DIR/x86_64 > /dev/null 2>&1 ;
- if $ISDEPENDS ; then cd $CI_PROJECT_DIR/x86_64 && echo $DEPENDENCIES_PACKAGE > file.txt && sudo pacman -U $(< file.txt xargs) --noconfirm ; fi;
```

Originalmente era sudo -H -i -u aur bash -e -x
Links: https://stackoverflow.com/q/8633461/9302545

```yml
$CI_PROJECT_DIR/templates/handler.sh
- if [ "$ISDEPENDS" == "true" ]; then cd $CI_PROJECT_DIR/x86_64 && echo ${DEPENDENCIES_PACKAGE} > ${CI_PROJECT_DIR}/x86_64/file.txt && sudo pacman -U --noconfirm `xargs -n 1 -I{} echo {} <${CI_PROJECT_DIR}/x86_64/file.txt`; fi
- cd $CI_PROJECT_DIR && curl -LO https://gitlab.com/carlosal1015/$CI_PROJECT_NAME/-/raw/master/packages.x86_64
- echo ${PACKAGE} > packages.x86_64 && cat packages.x86_64
- if [ "$ISAUR" == "false" ]; then
cd $CI_PROJECT_DIR && curl -LO https://gitlab.com/carlosal1015/$PACKAGE/-/raw/main/PKGBUILD > /dev/null 2>&1 ;
makepkg -src --noconfirm > /dev/null 2>&1 ;
mkdir -p x86_64 && mv $PACKAGE*.pkg.tar.zst x86_64 > /dev/null 2>&1 ;
else
xargs -I{} curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/{}.tar.gz <packages.x86_64 > /dev/null 2>&1 ;
for file in *.tar.gz; do tar -xvf \$file && rm \$file; done > /dev/null 2>&1 ;
while read in; do cd \$in && makepkg -src --noconfirm && cd ..; done <packages.x86_64 > /dev/null 2>&1 ;
mkdir -p x86_64 && mv **/\$PACKAGE*.pkg.tar.zst x86_64 ;
xargs -I{} rm -rf {} <packages.x86_64 ;
fi
- EOS
- pacman -Qtdq | xargs -r pacman --noconfirm -Rcns > /dev/null 2>&1
```

[](https://stackoverflow.com/a/41236640/9302545)

[](https://unix.stackexchange.com/questions/48535/can-grep-return-true-false-or-are-there-alternative-methods/48536)

[](https://github.com/archlinuxcn/repo/tree/master/archlinuxcn/pacman-static)

[](https://github.com/bianjp/version-watcher)
[](https://github.com/lilydjwg/nvchecker)
