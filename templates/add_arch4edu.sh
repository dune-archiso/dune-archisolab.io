#!/usr/bin/env bash

# Copyright (C) 2022 - present Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .
# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2016

function add_arch4edu_repository() {
  # https://github.com/arch4edu/arch4edu/wiki/Add-arch4edu-to-your-Archlinux
  local GPG_KEY=7931B6D628C8D3BA # Jingbei Li <i@jingbei.li>
  sudo pacman-key --recv-keys ${GPG_KEY}
  sudo pacman-key --finger ${GPG_KEY}
  sudo pacman-key --init
  sudo pacman-key --lsign-key ${GPG_KEY}
  local MIRROR="https://raw.githubusercontent.com/arch4edu/mirrorlist/master/mirrorlist.arch4edu"
  echo -e '\n[arch4edu]\nServer = https://repository.arch4edu.org/$arch\nInclude = /etc/pacman.d/arch4edumirrorlist' | sudo tee -a /etc/pacman.conf
  curl -s ${MIRROR} | sed 's/^Server/#Server/' | cut -c2- | sudo tee -a /etc/pacman.d/arch4edumirrorlist >/dev/null 2>&1
  # echo -e 'Server = https://mirrors.tencent.com/arch4edu/$arch\nServer = https://mirrors.aliyun.com/arch4edu/$arch\nServer = https://mirrors.bfsu.edu.cn/arch4edu/$arch\nServer = https://mirrors.nju.edu.cn/arch4edu/$arch\nServer = https://mirror.lesviallon.fr/arch4edu/$arch\nServer = https://mirrors.pinganyun.com/arch4edu/$arch\nServer = https://mirrors.sau.edu.cn/arch4edu/$arch\nServer = https://mirror.iscas.ac.cn/arch4edu/$arch\nServer = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch\nServer = https://at.arch4edu.mirror.kescher.at/$arch\nServer = https://keybase.pub/arch4edu/$arch\nServer = https://mirrors.ynu.edu.cn/arch4edu/$arch\nServer = https://pkg.fef.moe/arch4edu/$arch\nServer = https://mirror.sunred.org/arch4edu/$arch' | sudo tee -a /etc/pacman.d/arch4edumirrorlist
  # https://bbs.archlinux.org/viewtopic.php?id=164773
  sudo chmod 644 /etc/pacman.d/arch4edumirrorlist
}

if grep -Fxq "[arch4edu]" /etc/pacman.conf; then
  echo "[arch4edu] database already registered"
else
  add_arch4edu_repository
fi
