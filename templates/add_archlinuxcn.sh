#!/usr/bin/env bash

# Copyright (C) 2022 - present Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .
# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2016

function add_archlinuxcn_repository() {
  local GPG_KEY=F9F9FA97A403F63E # lilac (build machine) <lilac@build.archlinuxcn.org>
  sudo pacman-key --recv-keys ${GPG_KEY}
  sudo pacman-key --finger ${GPG_KEY}
  sudo pacman-key --init
  sudo pacman-key --lsign-key ${GPG_KEY}
  # https://github.com/archlinuxcn/repo/issues/3557
  sudo pacman-key --lsign-key "farseerfc@archlinux.org"
  local MIRROR="https://raw.githubusercontent.com/archlinuxcn/mirrorlist-repo/master/archlinuxcn-mirrorlist"
  echo -e '\n[archlinuxcn]\nServer = https://repo.archlinuxcn.org/$arch\nInclude = /etc/pacman.d/archlinuxcnmirrorlist' | sudo tee -a /etc/pacman.conf
  curl -s ${MIRROR} | cut -c2- | sudo tee -a /etc/pacman.d/archlinuxcnmirrorlist >/dev/null 2>&1
  # echo -e 'Server = https://mirrors.bfsu.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.cloud.tencent.com/archlinuxcn/$arch\nServer = https://mirrors.163.com/archlinux-cn/$arch\nServer = https://mirrors.aliyun.com/archlinuxcn/$arch\nServer = https://repo.huaweicloud.com/archlinuxcn/$arch\nServer = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.ustc.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.hit.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.zju.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.cqu.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.sjtug.sjtu.edu.cn/archlinux-cn/$arch\nServer = https://mirrors.nju.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.dgut.edu.cn/archlinuxcn/$arch\nServer = https://mirrors.sustech.edu.cn/archlinuxcn/$arch\nServer = https://archlinux.ccns.ncku.edu.tw/archlinuxcn/$arch\nServer = https://mirror.xtom.com.hk/archlinuxcn/$arch\nServer = https://mirror.xtom.com/archlinuxcn/$arch\nServer = https://mirror.xtom.nl/archlinuxcn/$arch\nServer = https://mirror.xtom.de/archlinuxcn/$arch\nServer = https://mirror.xtom.ee/archlinuxcn/$arch\nServer = https://mirrors.ocf.berkeley.edu/archlinuxcn/$arch' | sudo tee -a /etc/pacman.d/archlinuxcnmirrorlist
  # https://bbs.archlinux.org/viewtopic.php?id=164773
  sudo chmod 644 /etc/pacman.d/archlinuxcnmirrorlist
}

if grep -Fxq "[archlinuxcn]" /etc/pacman.conf; then
  echo "[archlinuxcn] database already registered"
else
  add_archlinuxcn_repository
fi
