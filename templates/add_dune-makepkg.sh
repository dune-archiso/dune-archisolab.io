#!/usr/bin/env bash

# Copyright (C) 2022 - present Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2016

function add_dune-makepkg_repository() {
  local GPG_KEY=6225FD2615EB3DEE # Carlos Aznarán <caznaranl@uni.pe>
  sudo pacman-key --recv-keys ${GPG_KEY}
  sudo pacman-key --finger ${GPG_KEY}
  sudo pacman-key --init
  sudo pacman-key --lsign-key ${GPG_KEY}
  echo -e '\n[dune-makepkg]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/testing/aur/dune-makepkg/$arch' | sudo tee -a /etc/pacman.conf
}

if grep -Fxq "[dune-makepkg]" /etc/pacman.conf; then
  echo "[dune-makepkg] database already registered"
else
  add_dune-makepkg_repository
fi
