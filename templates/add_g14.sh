#!/usr/bin/env bash

# Copyright (C) 2022 - present Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .
# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/dune-archiso.gitlab.io is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/dune-archiso.gitlab.io .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2016

function add_g14_repository() {
  # https://asus-linux.org/guides/arch-guide
  local GPG_KEY=8B15A6B0E9A3FA35 # dragonn <dragonn@op.pl>
  sudo pacman-key --recv-keys ${GPG_KEY}
  sudo pacman-key --finger ${GPG_KEY}
  sudo pacman-key --init
  sudo pacman-key --lsign-key ${GPG_KEY}
  echo -e '\n[g14]\nServer = https://arch.asus-linux.org\nInclude = /etc/pacman.d/g14mirrorlist' | sudo tee -a /etc/pacman.conf
  echo -e 'Server = https://naru.jhyub.dev/$repo' | sudo tee -a /etc/pacman.d/g14mirrorlist
  sudo chmod 644 /etc/pacman.d/g14mirrorlist
}

if grep -Fxq "[g14]" /etc/pacman.conf; then
  echo "[g14] database already registered"
else
  add_g14_repository
fi
