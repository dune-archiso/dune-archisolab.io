#!/usr/bin/env bash

# shellcheck disable=SC2046

set -e -u

# Update repo (human readable) of a fresh package
sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1

# Show the size (human readable) of a fresh package
du -h "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add the package ${MODULE}-${DUNE_VERSION}-1-${ARCHITECTURE}.pkg.tar.zst to the system and install the required dependencies from sync repositories
sudo pacman -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1

# Query the package database and display information on a given package
pacman -Qipl "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add contributed scripts and tools for pacman systems (e.g. pactree)
sudo pacman -S pacman-contrib python-pip --noconfirm >/dev/null 2>&1

DUNE_VERSION=$(echo "${DUNE_VERSION}" | tr -d '.')

printf 'y\n' | amdisproject "${PROJECT}" "${DEPENDENCIES}" "${VERSION}" "${MAINTAINER}"
grep -F Depends "${PROJECT}"/dune.module
# export CFLAGS="-I/usr/include/tirpc"
# export LDFLAGS="-ldl -lm -ltirpc"
# CMAKE_EXE_LINKER_FLAGS
# CMAKE_MODULE_LINKER_FLAGS
# CMAKE_SHARED_LINKER_FLAGS
# -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON
# -DCMAKE_CXX_COMPILE_OPTIONS_PIE="-fPIC"
# -E env LDFLAGS="-ldl -lm -ltirpc"
# https://stackoverflow.com/a/2673355/9302545
cmake \
  -S "${PROJECT}" \
  -B build \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  -Wno-dev

cmake --build build
# CMAKE_FLAGS='-DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native
# -DCMAKE_CXX_COMPILE_OPTIONS_PIE:STRING="-fPIC" \
# -DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE
# --mca opal_warn_on_missing_libcuda 0

if hash mpirun 2>/dev/null; then
  mpirun -np 1 --oversubscribe ./build/src/"${PROJECT}"
else
  ./build/src/"${PROJECT}"
fi
