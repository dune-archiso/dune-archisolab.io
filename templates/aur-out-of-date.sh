#!/usr/bin/env bash

# shellcheck disable=SC2086,SC2046,SC2155

function check_aur_packages() {
  local URL=https://gitlab.com/dune-archiso/repository/${CI_PROJECT_NAME}/-/raw/main/packages.x86_64
  local lista=$(curl -s $URL | tr '\n' ' ')
  aur-out-of-date -pkg $(echo "$lista")
}

if hash aur-out-of-date 2>/dev/null; then
  check_aur_packages
else
  echo "Run yay -Syu aur-out-of-date"
fi
