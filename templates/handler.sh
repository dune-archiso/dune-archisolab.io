#!/usr/bin/env bash

# shellcheck disable=SC2155,SC2046

set -e -u

echo "Bash version: ${BASH_VERSION}"

umask 0077
export CONTINUOUS_INTEGRATION_SYSTEM="gitlab" TIMESTAMP=$(date +%Y%m%d%H%M%S) DEFAULT_TARGET="debug"
export PREFIX="${HOME}/usr" SYSCONFDIR="${HOME}/etc/conf" LOCALSTATEDIR="${HOME}/var"

if [ "$ISSIGNED" == "true" ]; then
  export GPG_TTY=$(tty)
  cp "${CRYPT_KEY}" mytoken.txt
  cat mytoken.txt | base64 -d >key-file
  gpg --pinentry-mode=loopback -v --import <(cat key-file) # --passphrase "$X"
  rm mytoken.txt key-file
fi

if [ "$ISARCH4EDU" == "true" ]; then
  curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_arch4edu.sh | bash
fi

if [ "$ISARCHLINUXCN" == "true" ]; then
  curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_archlinuxcn.sh | bash
fi

sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1

if [ "$ISDEPENDS" == "true" ]; then
  # The folder x86_64 (no public/x86_64 yet) should have some *-x86_64.pkg.tar.zst (at least one).
  cd "${CI_PROJECT_DIR}"/x86_64
  # This must copy all the *-x86_64.pkg.tar.zst in the before stage (be must non-empty).
  echo "${DEPENDENCIES_PACKAGE}" >"${CI_PROJECT_DIR}"/x86_64/file.txt
  if [ "$ISSIGNED" == "true" ]; then
    export GPG_KEY=6225FD2615EB3DEE # Carlos Aznarán <caznaranl@uni.pe>
    sudo pacman-key --init
    sudo pacman-key --recv-keys ${GPG_KEY}
    sudo pacman-key --finger ${GPG_KEY}
    sudo pacman-key --lsign-key ${GPG_KEY}
  fi
  sudo pacman -U --noconfirm $(xargs -I{} echo {} <"${CI_PROJECT_DIR}"/x86_64/file.txt) #>/dev/null 2>&1 pacman --debug
  rm -rf "${CI_PROJECT_DIR}"/x86_64
fi

if [ "$ISAUR" == "false" ]; then
  cd "${CI_PROJECT_DIR}"
  # They must be point to the repository
  # dune/PKGBUILDS/{core,disc,extension,grid,tutorial,alberta3,arpackpp-git}/PKGBUILD.
  # Only works in the repositories dune-archiso-{core,extra}.
  # gpg --receive-keys EEC04475390E9CA92D2D6CE1C69045DEA6EBA3C9
  curl -LO https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/"${MODULE}"/PKGBUILD >/dev/null 2>&1
  if [ "$ISDUNEOPTS" == "true" ]; then
    curl -LO https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/"${MODULE}"/"${MODULE}".opts >/dev/null 2>&1
  fi
  makepkg -src --cleanbuild --noconfirm #>/dev/null 2>&1
  if [ "$ISSIGNED" == "true" ]; then
    for pkg in *.pkg.tar.zst; do
      gpg --pinentry-mode=loopback --detach-sign "${pkg}" # --passphrase "$X"
    done
  fi
  mkdir -p x86_64
  mv ./*.pkg.tar.zst* x86_64
else
  cd "${CI_PROJECT_DIR}"
  if [ "$ISBUNDLE" == "true" ]; then
    # This job is for example: build_tools.
    curl -LO https://gitlab.com/"${CI_PROJECT_PATH}"/-/raw/main/packages.x86_64 #>/dev/null 2>&1
    printf "emacs-git\nimagine-git\njupyter-gnuplot_kernel\njupyter-metakernel" >blacklist && grep -vf blacklist packages.x86_64 >whitelist
    mv whitelist packages.x86_64
    xargs -I{} curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/{}.tar.gz <packages.x86_64
  else
    # Only build one PKGBUILD from AUR.
    echo "${PACKAGE}" >packages.x86_64
  fi
  cat packages.x86_64
  xargs -I{} curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/{}.tar.gz <packages.x86_64 #>/dev/null 2>&1

  for file in *.tar.gz; do
    tar -xvf "${file}"
    rm "${file}"
  done >/dev/null 2>&1

  while read -r in; do
    cd "${in}"
    makepkg -src --cleanbuild --noconfirm #>/dev/null 2>&1
    if [ "$ISSIGNED" == "true" ]; then
      for pkg in *.pkg.tar.zst; do
        gpg --pinentry-mode=loopback --detach-sign "${pkg}" # --passphrase "$X"
      done
    fi
    cd ..
  done <packages.x86_64 #>/dev/null 2>&1
  mkdir -p x86_64
  mv ./**/*.pkg.tar.* x86_64 #>/dev/null 2>&1
  xargs -I{} rm -rf {} <packages.x86_64
fi
