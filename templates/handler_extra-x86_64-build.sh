#!/usr/bin/env bash

# shellcheck disable=SC2155,SC2046

set -e -u

echo "Bash version: ${BASH_VERSION}"

umask 0077
export CONTINUOUS_INTEGRATION_SYSTEM="gitlab" TIMESTAMP=$(date +%Y%m%d%H%M%S) DEFAULT_TARGET="debug"
export PREFIX="${HOME}/usr" SYSCONFDIR="${HOME}/etc/conf" LOCALSTATEDIR="${HOME}/var"

sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1

if [ "$ISDEPENDS" == "true" ]; then
  # The folder x86_64 (no public/x86_64 yet) should have some *-x86_64.pkg.tar.zst (at least one).
  cd "${CI_PROJECT_DIR}"/x86_64
  # This must copy all the *-x86_64.pkg.tar.zst in the before stage (be must non-empty).
  echo "${DEPENDENCIES_PACKAGE}" >"${CI_PROJECT_DIR}"/x86_64/file.txt
  sudo pacman -U --noconfirm $(xargs -I{} echo {} <"${CI_PROJECT_DIR}"/x86_64/file.txt) #>/dev/null 2>&1
  rm -rf "${CI_PROJECT_DIR}"/x86_64
fi

if [ "$ISAUR" == "false" ]; then
  cd "${CI_PROJECT_DIR}"
  # They must be point to the repository
  # dune/PKGBUILDS/{core,disc,extension,grid,tutorial,alberta3,arpackpp-git}/PKGBUILD.
  # Only works in the repositories dune-archiso-{core,extra}.
  # gpg --receive-keys 3880BDA725DE92E7BE7C1A2F6225FD2615EB3DEE
  curl -LO https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/"${MODULE}"/PKGBUILD >/dev/null 2>&1
  if [ "$ISDUNEOPTS" == "true" ]; then
    curl -LO https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/"${MODULE}"/"${MODULE}".opts >/dev/null 2>&1
  fi
  extra-x86_64-build
  # makepkg -sr --cleanbuild --noconfirm #>/dev/null 2>&1
  mkdir -p x86_64
  mv ./*.pkg.tar.zst x86_64
else
  mkdir -p "$HOME"/chroot
  curl -Ls https://aur.archlinux.org/cgit/aur.git/snapshot/python-portalocker.tar.gz | tar -xvzf - -C "$HOME"
  export CHROOT=$HOME/chroot
  # mkarchroot $CHROOT/root base-devel
  # paru -Syyu --noconfirm >/dev/null 2>&1
  # paru -S ${PACKAGE} --noconfirm # --removemake
  # mkdir -p x86_64
  # mv $HOME/.cache/paru/clone/*/${PACKAGE}-*.pkg.tar.zst x86_64
  # zstdcat -l x86_64/${PACKAGE}-*.pkg.tar.zst
  # du -h x86_64/${PACKAGE}-*.pkg.tar.zst
  # tar tf x86_64/${PACKAGE}-*.pkg.tar.zst
fi
