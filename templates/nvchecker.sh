#!/usr/bin/env bash

function check_new_packages() {
  nvchecker -c config_file.toml #-l debug
  # nvtake -c config_file.toml
  # nvcmp -c config_file.toml
}

if hash nvchecker 2>/dev/null; then
  check_new_packages
else
  echo "Run yay -Syyu nvchecker"
fi
