#!/usr/bin/env bash

# https://gitlab.com/protist/pacolog
sudo pacman -Syuq --noconfirm --needed
URL=https://foo.bar
curl -O $URL
makepkg -si --noconfirm
