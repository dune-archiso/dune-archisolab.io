#!/usr/bin/env bash

# shellcheck disable=SC2046

set -e -u

# Show the size (human readable) of a fresh package
du -h "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add the package ${MODULE}-${DUNE_VERSION}-1-${ARCHITECTURE}.pkg.tar.zst to the system and install the required dependencies from sync repositories
sudo pacman -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1

# Query the package database and display information on a given package
pacman -Qipl "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add contributed scripts and tools for pacman systems (e.g. pactree)
sudo pacman -S pacman-contrib python-pip --noconfirm >/dev/null 2>&1
# gnuplot

EXECUTABLE_DIR=/tmp/makepkg/dune-precice-git/src/dune-adapter/dune-precice-howto/build-cmake/examples

git clone --filter=blob:none --depth=1 --branch develop https://github.com/precice/tutorials.git
cd tutorials/perpendicular-flap/solid-dune
mv ${EXECUTABLE_DIR}/dune-perpendicular-flap* .
./run.sh
pwd
ls -la
# Package dependency tree viewer: limits the number of levels of dependency to show
pactree -d 3 "${MODULE}"
pacman -Q $(pactree -u "${MODULE}")

DUNE_VERSION=$(echo "${DUNE_VERSION}" | tr -d '.')

if [[ $MODULE == *git || $DUNE_VERSION -ge 280 ]]; then
  DEPENDENCIES=${MODULE%%-git}
  DUNE_PYTHON_MODULE=$(echo "$DEPENDENCIES" | tr - .)
  if [[ $(pip list | grep "$DUNE_PYTHON_MODULE") ]]; then
    python -c "import sys; print('.'.join(str(v) for v in sys.version_info[:3]))"
    pip list
    python -c "import ${DUNE_PYTHON_MODULE};"
    pactree -d 3 python-"${MODULE}"
    pacman -Q $(pactree -u python-"${MODULE}")
  fi
fi

# if [[ $(pip list | grep dune.python) ]]; then
#   python -c "import dune.common; import dune.geometry; import dune.istl;"
# fi

printf 'y\n' | duneproject "${PROJECT}" "${DEPENDENCIES}" "${VERSION}" "${MAINTAINER}"
grep -F Depends "${PROJECT}"/dune.module
# export CFLAGS="-I/usr/include/tirpc"
# export LDFLAGS="-ldl -lm -ltirpc"
# CMAKE_EXE_LINKER_FLAGS
# CMAKE_MODULE_LINKER_FLAGS
# CMAKE_SHARED_LINKER_FLAGS
# -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON
# -DCMAKE_CXX_COMPILE_OPTIONS_PIE="-fPIC"
# -E env LDFLAGS="-ldl -lm -ltirpc"
# https://stackoverflow.com/a/2673355/9302545
cmake \
  -S "${PROJECT}" \
  -B build \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  -Wno-dev

cmake --build build
# CMAKE_FLAGS='-DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native
# -DCMAKE_CXX_COMPILE_OPTIONS_PIE:STRING="-fPIC" \
# -DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE
# --mca opal_warn_on_missing_libcuda 0

if hash mpirun 2>/dev/null; then
  mpirun -np 1 --oversubscribe ./build/src/"${PROJECT}"
else
  ./build/src/"${PROJECT}"
fi
