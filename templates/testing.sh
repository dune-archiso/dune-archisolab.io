#!/usr/bin/env bash
# shellcheck disable=SC2155,SC2046

set -e -u

umask 0077
export CONTINUOUS_INTEGRATION_SYSTEM="gitlab" TIMESTAMP=$(date +%Y%m%d%H%M%S) DEFAULT_TARGET="debug"
export PREFIX="${HOME}/usr" SYSCONFDIR="${HOME}/etc/conf" LOCALSTATEDIR="${HOME}/var"

sudo pacman -Syuq --needed --noconfirm >/dev/null 2>&1

if [ "$ISDEPENDS" == "true" ]; then
  # The folder x86_64 (no public/x86_64 yet) should have some *-x86_64.pkg.tar.zst (at least one).
  cd "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  # This must copy all the *-x86_64.pkg.tar.zst in the before stage (be must non-empty).
  echo "${DEPENDENCIES_PACKAGE}" >"${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/file.txt
  sudo pacman -U --noconfirm $(xargs -I{} echo {} <"${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/file.txt) >/dev/null 2>&1
  rm -rf "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
fi

cd "${CI_PROJECT_DIR}"
curl -LO "${PKGBUILD_URL}"

if [ "$ISDUNEOPTS" == "true" ]; then
  curl -LO "${OPTS_URL}" >/dev/null 2>&1
fi

makepkg -src --cleanbuild --noconfirm >/dev/null 2>&1

ls -la

mkdir -p "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
mv ./*.pkg.tar.zst "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"

# Show the size of a fresh package, in human readable units
du -h "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add fresh package to the system and install the required dependencies from sync repositories
sudo pacman -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/"${MODULE}"-*-1-"${ARCHITECTURE}".pkg.tar.zst --noconfirm >/dev/null 2>&1

# Query the package database and display information on a given package
pacman -Qil "${MODULE}"

# Add contributed scripts and tools for pacman systems (e.g. pactree)
sudo pacman -S pacman-contrib --noconfirm >/dev/null 2>&1

DUNE_VERSION=$(echo "${DUNE_VERSION}" | tr -d '.')

if [[ $MODULE == *git || $DUNE_VERSION -ge 271 ]]; then
  printf 'y\n' | duneproject "${PROJECT}" "${DEPENDENCIES}" "${VERSION}" "${MAINTAINER}"
  grep -F Depends "${PROJECT}"/dune.module

  cmake \
    -S "${PROJECT}" \
    -B build \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -Wno-dev

  cmake --build build

  if hash mpirun 2>/dev/null; then
    mpirun -np 1 --mca opal_warn_on_missing_libcuda 0 --oversubscribe ./build/src/"${PROJECT}"
  else
    ./build/src/"${PROJECT}"
  fi

fi
