#!/usr/bin/env bash

set -e -u

# Show the size of a fresh package, in human readable units
du -h "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst

# Add fresh package to the system and install the required dependencies from sync repositories
sudo pacman -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/"${MODULE}"-"${DUNE_VERSION}"-1-"${ARCHITECTURE}".pkg.tar.zst --noconfirm >/dev/null 2>&1

# Query the package database and display information on a given package
pacman -Qil "${MODULE}"

# Add contributed scripts and tools for pacman systems (e.g. pactree)
sudo pacman -S pacman-contrib --noconfirm >/dev/null 2>&1

printf 'y\n' | duneproject "${PROJECT}" "${DEPENDENCIES}" "${VERSION}" "${MAINTAINER}"
grep -F Depends "${PROJECT}"/dune.module

cmake \
  -S "${PROJECT}" \
  -B build \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  -Wno-dev

cmake --build build

if hash mpirun 2>/dev/null; then
  mpirun -np 1 --mca opal_warn_on_missing_libcuda 0 --oversubscribe ./build/src/"${PROJECT}"
else
  ./build/src/"${PROJECT}"
fi
