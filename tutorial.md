## ubuntu 24.04

```console
$ docker run -it --rm ubuntu:24.04
# apt-get update
# apt-get install libdune-pdelab-dev -y
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Package libdune-pdelab-dev is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'libdune-pdelab-dev' has no installation candidate
```

## openSUSE Tumbleweed

```console
$ docker run -it --rm opensuse/tumbleweed
# zypper ar -f https://download.opensuse.org/repositories/science/openSUSE_Tumbleweed/science.repo && zypper --gpg-auto-import-keys ref
# zypper install -y dune-pdelab-devel
Loading repository data...
Reading installed packages...
'dune-pdelab-devel' not found in package names. Trying capabilities.
No provider of 'dune-pdelab-devel' found.
# #rpm -ql dune-pdelab-devel
```

## FreeBSD 14.0

```console
$ wget2 https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.0/FreeBSD-14.0-RELEASE-amd64-disc1.iso.xz
$ xz --decompress FreeBSD-14.0-RELEASE-amd64-disc1.iso.xz
```

[](https://forums.freebsd.org/threads/sudoers-file-location-scripting-entries.60436)
